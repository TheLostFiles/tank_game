﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject instantiatedPlayerTank;
    public GameObject instantiatedPlayerTank2;
    public GameObject playerTankPrefab;
    public List<GameObject> playerSpawnPoints;

    public List<GameObject> instantiatedEnemyTanks;
    public List<GameObject> enemyTanksPrefabs;
    public List<GameObject> enemySpawnPoints;

    public GameObject levelObject;

    public bool StartGame;

    public int highScore;
    public float fxVolume;
    public float musicVolume;
    public List<ScoreData> highScores;

    public enum MapType { Seeded, Random, MapOfTheDay }
    public MapType mapType = MapType.Random;
    public int mapSeed;



    public int numberOfPlayers = 1;

    public bool p1Dead;
    public bool p2Dead;

    void Awake() // 
    {
        if (instance == null) // checks if instance is null
        {
            instance = this; // makes instance this
        }
        else
        {
            Debug.LogError("ERROR: There can only be one GameManager."); //display message
            Destroy(gameObject); // it go boom
        }
        DontDestroyOnLoad(gameObject); // makes sure it doesn't die when you move scenes
    }

    public void Start()
    {
        highScores = new List<ScoreData>();
        LoadPrefs();
        highScores.Sort();
        highScores.Reverse();
        highScores = highScores.GetRange(0, 5);
    }
    public void Update()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(1))
        {
            if(StartGame == false)
            {
                StartGame = true;
                MapGenerator mapGenerator = levelObject.GetComponent<MapGenerator>();
                mapGenerator.StartGame();
            }
        }
    }

    public GameObject RandomSpawnPoint(List<GameObject> spawnPoints) // makes the spawn points random
    {
       int spawnToGet = UnityEngine.Random.Range(0, spawnPoints.Count - 1); // makes a random range for the spawn points 
       return spawnPoints[spawnToGet];
    }

    public void SpawnPlayer(GameObject spawnPoint)
    {
        if(numberOfPlayers == 1)
        {
            instantiatedPlayerTank = Instantiate(playerTankPrefab, spawnPoint.transform.position, Quaternion.identity); // makes the player live
        }
        else
        {
            instantiatedPlayerTank = Instantiate(playerTankPrefab, spawnPoint.transform.position, Quaternion.identity); // makes the player live
            GameObject spawnPoint2 = RandomSpawnPoint(playerSpawnPoints);
            instantiatedPlayerTank2 = Instantiate(playerTankPrefab, spawnPoint2.transform.position, Quaternion.identity); // makes the player live
            instantiatedPlayerTank.GetComponent<TankData>().playerNumber = 1;
            instantiatedPlayerTank2.GetComponent<TankData>().playerNumber = 2;
            instantiatedPlayerTank2.GetComponent<InputController>().input = InputController.InputScheme.arrowKeys;
        }
        
    }

    public void SpawnEnemies() 
    {
        for (int i = 0; i < enemyTanksPrefabs.Count; i++)
        {
            GameObject instantiatedTank = Instantiate(enemyTanksPrefabs[i], RandomSpawnPoint(enemySpawnPoints).transform.position, Quaternion.identity); // makes them a place to appear and then they appear
            instantiatedEnemyTanks.Add(instantiatedTank); // adds the new tanks to a list
        }
    }
    public void LoadPrefs()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScore = PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            highScore = 0;
        }

        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            musicVolume = PlayerPrefs.GetFloat("MusicVolume");
        }
        else
        {
            musicVolume = 100.0f;
        }

        if (PlayerPrefs.HasKey("FXVolume"))
        {
            fxVolume = PlayerPrefs.GetFloat("FXVolume");
        }
        else
        {
            fxVolume = 100.0f;
        }

        if (PlayerPrefs.HasKey("Score1"))
        {
            highScores.Add(new ScoreData(PlayerPrefs.GetString("Name1"), PlayerPrefs.GetFloat("Score1")));
        }
        else
        {
            highScores.Add(new ScoreData("ELY",0));
        }
        if (PlayerPrefs.HasKey("Score2"))
        {
            highScores.Add(new ScoreData(PlayerPrefs.GetString("Name2"), PlayerPrefs.GetFloat("Score2")));
        }
        else
        {
            highScores.Add(new ScoreData("ELY", 0));
        }
        if (PlayerPrefs.HasKey("Score3"))
        {
            highScores.Add(new ScoreData(PlayerPrefs.GetString("Name3"), PlayerPrefs.GetFloat("Score3")));
        }
        else
        {
            highScores.Add(new ScoreData("ELY", 0));
        }
        if (PlayerPrefs.HasKey("Score4"))
        {
            highScores.Add(new ScoreData(PlayerPrefs.GetString("Name4"), PlayerPrefs.GetFloat("Score4")));
        }
        else
        {
            highScores.Add(new ScoreData("ELY", 0));
        }
        if (PlayerPrefs.HasKey("Score"))
        {
            highScores[0].name = PlayerPrefs.GetString("Name1");
            highScores[0].score = PlayerPrefs.GetFloat("Score1");
        }
        else
        {
            highScores.Add(new ScoreData("ELY", 0));
        }
    }

    public void SavePrefs()
    {
        PlayerPrefs.SetInt("HighScore", highScore);

        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
        PlayerPrefs.SetFloat("FXVolume", fxVolume);

        PlayerPrefs.SetString("Name1", highScores[0].name);
        PlayerPrefs.SetString("Name2", highScores[1].name);
        PlayerPrefs.SetString("Name3", highScores[2].name);
        PlayerPrefs.SetString("Name4", highScores[3].name);
        PlayerPrefs.SetString("Name5", highScores[4].name);

        PlayerPrefs.SetFloat("Score1", highScores[0].score);
        PlayerPrefs.SetFloat("Score2", highScores[1].score);
        PlayerPrefs.SetFloat("Score3", highScores[2].score);
        PlayerPrefs.SetFloat("Score4", highScores[3].score);
        PlayerPrefs.SetFloat("Score5", highScores[4].score);

        PlayerPrefs.Save();
    }

    private void OnApplicationQuit()
    {
        SavePrefs();

    }
}
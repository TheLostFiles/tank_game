﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Threading.Tasks;
using UnityEditor.VersionControl;
using UnityEngine;

[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankShooter))]
public class AIController : MonoBehaviour
{
    public enum Personalities {Inky, Blinky, Pinky, Clyde }
    public Personalities personalities;



    public enum AIState { Chase, ChaseAndFire, CheckForFlee, Flee, Rest, Patrol}
    public AIState aiState = AIState.Chase;

    public float fleeDistance = 1.0f;
    public bool canShoot = true;
    private float timeUntilCanShoot;



    public enum LoopType { Stop, Loop, PingPong }
    public LoopType loopType = LoopType.Stop;

    public Transform[] waypoints;
    private int currentWaypoint = 0;
    public float closeEnough = 1.0f;
    public bool isPatrolForward = true;



    public enum AvoidanceStage { None, Rotate, Move};
    public AvoidanceStage avoidanceStage;

    public float AvoidanceTime = 2.0f;
    private float exitTime;



    private float stateEnterTime;
    private float healthRegenPerSecond = 2f;
    public float aiSenseRadius;

    public float hearingDistance = 25.0f;
    public float FOVAngle = 45.0f;
    public float inSightsAngle = 10.0f;



    public GameObject player;
    public Transform target;

    private TankData data;
    private TankMotor motor;
    private TankShooter shooter;

    private Transform tf;

    void Start()
    {
        tf = gameObject.GetComponent<Transform>(); 
        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<TankMotor>();
        shooter = gameObject.GetComponent<TankShooter>();
    }

    void Update()
    {

        switch (personalities)
        {
            case Personalities.Inky:
                Inky(); // inky
                break;
            case Personalities.Blinky:
                Blinky(); // blinky
                break;
            case Personalities.Pinky:
                Pinky(); // pinky
                break;
            case Personalities.Clyde:
                Clyde(); // clyde
                break;
        }
    }

    private void Inky()
    {
        switch (aiState)
        {
            case AIState.Patrol:
                Shoot();
                if (avoidanceStage != AvoidanceStage.None) // checks if avoidance stage is not none
                {
                    Avoid(); // avoid 
                }
                else
                {
                    Patrol(); // patrol
                }
                break;
        }
    }

    private void Blinky()
    {
        switch (aiState)
        {
            case AIState.Chase: // chase 
                if (avoidanceStage != AvoidanceStage.None) // checks if avoidance stage is not none
                {
                    Avoid();// avoid 
                }
                else
                {
                    Chase(player); // chase 
                }

                if (data.health < data.maxHealth * 0.5) // checks if the health is half of max
                {
                    ChangeState(AIState.CheckForFlee); // changes state to check for flee
                }
                else if (PlayerIsInRange()) // checks if player is in range
                {
                    ChangeState(AIState.ChaseAndFire); // changes state to chase and fire 
                }
                break;

            case AIState.ChaseAndFire: // chase and fire
                if (avoidanceStage != AvoidanceStage.None) // checks if avoidance stage is not none
                {
                    Avoid();// avoid 
                }
                else
                {
                    Chase(player); // chase
                    Shoot(); // shoot
                }
                
                if (data.health < data.maxHealth * 0.5) // checks if the health is half of max
                {
                    ChangeState(AIState.CheckForFlee); // changes state to check for flee
                }
                else if (!PlayerIsInRange()) // checks if the player is not in range
                {
                    ChangeState(AIState.Chase); // changes state to chase
                }
                break;

            case AIState.CheckForFlee:

                if (PlayerIsInRange()) // checks if player is in range
                {
                    ChangeState(AIState.Flee); // changes state to flee
                }
                else
                {
                    ChangeState(AIState.Rest);// changes state to rest
                }
                break;

            case AIState.Flee:
                if (avoidanceStage != AvoidanceStage.None) // checks if avoidance stage is not none
                {
                    Avoid(); // avoid 
                }
                else
                {
                    Flee(player); // flee
                }
                
                // Wait 30 seconds then check for flee.
                if (Time.time >= stateEnterTime + 3f) // timer of 3 seconds
                {
                    ChangeState(AIState.CheckForFlee); // changes state to check for flee
                }
                break;

            case AIState.Rest:
                Rest();
                if (PlayerIsInRange()) // checks if player is in range
                {
                    ChangeState(AIState.Flee); // changes state to flee
                }
                else if (data.health >= data.maxHealth) // makes sure health doesn't go over 10
                {
                    ChangeState(AIState.Chase); // changes state to chase
                }
                break;

            case AIState.Patrol:
                Patrol(); // patrol

                break;

            default:
                break;
        }
    }

    private void Pinky()
    {
        switch (aiState)
        {
            case AIState.Chase: // chase
                if (avoidanceStage != AvoidanceStage.None) // checks if avoidance stage is not none
                {
                    Avoid(); // avoid
                }
                else
                {
                    Chase(player); // chase
                }
                if (PlayerIsInRange()) // checks if player is in range
                {
                    ChangeState(AIState.Flee); // changes state to flee
                }

                break;

            case AIState.Flee:
                if (avoidanceStage != AvoidanceStage.None) // checks if avoidance stage is not none
                {
                    Avoid();// avoid
                }
                else
                {
                    Flee(player); // flee
                }

                if (Time.time >= stateEnterTime + 7f) // timer of 7 seconds
                {
                    ChangeState(AIState.Chase); // changes state to chase
                }
                break;
        }
    }

    private void Clyde()
    {
        switch(aiState)
        {
            case AIState.Flee:
                if (avoidanceStage != AvoidanceStage.None) // checks if avoidance stage is not none
                {
                    Avoid();// avoid
                }
                else
                {
                    Flee(player); // flee
                }
                break;
        }
    }

    public bool CanMove(float speed)
    {
        RaycastHit hit; // makes rayc ast
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed)) // checks if it hits anything
        {
            if (!hit.collider.CompareTag("Enemy")) // checks if it hits enemy
            {
                return false;
            }
        }
        return true;
    }

    private void Avoid()
    {
        switch (avoidanceStage) // avoidance stages
        {
            case AvoidanceStage.None: // none
                break;

            case AvoidanceStage.Rotate:

                motor.Rotate(data.rotateSpeed); // rotates
                if (CanMove(data.moveSpeed)) // checks if you can move
                {
                    avoidanceStage = AvoidanceStage.Move; // moves state
                    exitTime = AvoidanceTime; // sets exit time to avoidance time
                }
                break;


            case AvoidanceStage.Move: // move

                if (CanMove(data.moveSpeed)) // checks if tou can move
                {
                    exitTime -= Time.deltaTime; // makes exit time go down
                    motor.Move(data.moveSpeed); // moves 

                    if (exitTime <= 0) // checks if exit time is 0
                    {
                        avoidanceStage = AvoidanceStage.None; // none state
                    }
                }
                else
                {
                    avoidanceStage = AvoidanceStage.Rotate; // rotate state
                }
                break;
        }
    }

    private void Chase(GameObject targetGameObject)
    {
        motor.RotateTowards(target.position, data.rotateSpeed); // rotates towards 

        if (CanMove(data.moveSpeed)) // checks if you can move
        {
            motor.Move(data.moveSpeed); // moves
        }
        else
        {
            avoidanceStage = AvoidanceStage.Rotate; // rotate state
        }
    }

    private void Flee(GameObject player)
    {
        // The vector from ai to target is target position minus our position.
        Vector3 vectorToTarget = target.position - tf.position;

        // We can flip this vector by -1 to get a vector AWAY from our target
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // Now, we can normalize that vector to give it a magnitude of 1
        vectorAwayFromTarget.Normalize();

        // A normalized vector can be multiplied by a length to make a vector of that length.
        vectorAwayFromTarget *= fleeDistance;

        // We can find the position in space we want to move to by adding our vector away from our AI to our AI's position.
        //     This gives us a point that is "that vector away" from our current position.
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.rotateSpeed);
        if (CanMove(data.moveSpeed)) // checks if you can move
        {
            motor.Move(data.moveSpeed); // moves
        }
        else
        {
            avoidanceStage = AvoidanceStage.Rotate; // rotate state
        }
    }

    private bool PlayerIsInRange()
    {
        if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius) // checks the distance of player to the AI
        {
            return true;
        }
        return false;
    }

    private void Rest()
    {
        data.health += healthRegenPerSecond * Time.deltaTime; // adds health per time
    }

    private void Shoot()
    {
        if (timeUntilCanShoot > 0) // checks for the time float
        {
            timeUntilCanShoot -= Time.deltaTime; // sets the countdown
        }
        else
        {
            canShoot = true; // switches the bool to true
        }
        if (canShoot) // checks if can shoot
        {
            shooter.Shoot(); // runs shoot
            canShoot = false; // sets shoot to false
            timeUntilCanShoot = data.fireRate; // sets the timer back
        }
    }

    public void ChangeState(AIState newState)
    {
        aiState = newState; // new AIstate
        stateEnterTime = Time.time; // makes state time time
    }

    void Patrol()
    {
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.rotateSpeed)) // turn towards next way point
        {
            motor.Move(0); // move but dont
        }
        else if(CanMove(data.moveSpeed)) // checks if can move
        {
            motor.Move(data.moveSpeed); // move
        }
        else
        {
            avoidanceStage = AvoidanceStage.Rotate; // pu into rotate state
        }
        // If we are close to the waypoint,
        //if (Vector3.Distance(transform.position, waypoints[currentWaypoint].position) < closeEnough)
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            switch (loopType)
            {
                case LoopType.Stop:
                    // Advance to the next waypoint, if we are still in range
                    if (currentWaypoint < waypoints.Length - 1) // 
                    {
                        currentWaypoint++;
                    }
                    break;
                case LoopType.Loop:
                    // Advance to the next waypoint, if we are still in range
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        currentWaypoint = 0;
                    }
                    break;
                case LoopType.PingPong:
                    if (isPatrolForward)
                    {// Advance to the next waypoint, if we are still in range
                        if (currentWaypoint < waypoints.Length - 1)
                        {
                            currentWaypoint++;
                        }
                        //swaps directions
                        else
                        {
                            isPatrolForward = false;
                            currentWaypoint--;
                        }
                    }
                    else
                    {
                        if (currentWaypoint > 0)
                        {
                            currentWaypoint--;
                        }
                        else
                        {
                            isPatrolForward = true;
                            currentWaypoint++;
                        }
                    }
                    break;
                default:
                    Debug.LogError("Loop type not implemented.");
                    break;
            }
        }
    }
}
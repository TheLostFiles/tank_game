﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankShooter))]
public class InputController : MonoBehaviour
{

    public enum InputScheme { WASD, arrowKeys } // makes an ENUM with wasd and arrowkeys
    public InputScheme input = InputScheme.WASD; // sets the default switch

    private TankData data;
    private TankMotor motor; 
    private TankShooter shooter;

    private float timeUntilCanShoot;
    public bool canShoot = true;

    // Start is called before the first frame update
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<TankMotor>();
        shooter = gameObject.GetComponent<TankShooter>();
    }

    // Update is called once per frame
    void Update()
    { // move speed of zero when no button is pressed

        if (timeUntilCanShoot > 0) // checks for the time float
        {
            timeUntilCanShoot -= Time.deltaTime; // sets the countdown
        }
        else 
        {
            canShoot = true; // switches the bool to true

        }

        switch (input) // switch
        {
            case InputScheme.WASD: // the WASD case
                if (canShoot) // checks if can shoot
                {
                    if (Input.GetKey(KeyCode.Space)) // checks for space input
                    {
                        shooter.Shoot(); // runs shoot
                        canShoot = false; // sets shoot to false
                        timeUntilCanShoot = data.fireRate; // sets the timer back
                    }
                }
                if (Input.GetKey(KeyCode.W)) // checks for input
                {
                    motor.Move(data.moveSpeed); // moves forward
                }
                if (Input.GetKey(KeyCode.S)) // checks for input
                {
                    motor.Move(-data.reverseSpeed); // moves back
                }
                if (Input.GetKey(KeyCode.D)) // checks for input
                {
                    motor.Rotate(data.rotateSpeed); // rotates right
                }
                if (Input.GetKey(KeyCode.A)) // checks for input
                {
                    motor.Rotate(-data.rotateSpeed); // rotates left
                }
                else
                {
                    motor.Move(0);// sets movement to 0
                }
                break;

            case InputScheme.arrowKeys:
                if (canShoot)
                {
                    if (Input.GetKey(KeyCode.RightControl))// checks for input
                    {
                        shooter.Shoot();// runs shoot
                        canShoot = false; // sets shoot to false
                        timeUntilCanShoot = data.fireRate; // sets the timer back
                    }
                }
                if (Input.GetKey(KeyCode.UpArrow)) // checks for input
                {
                    motor.Move(data.moveSpeed); // moves forward
                }
                if (Input.GetKey(KeyCode.DownArrow)) // checks for input
                {
                    motor.Move(-data.moveSpeed); // moves back
                }
                if (Input.GetKey(KeyCode.RightArrow)) // checks for input
                {
                    motor.Rotate(data.rotateSpeed); // rotates right
                }
                if (Input.GetKey(KeyCode.LeftArrow)) // checks for input
                {
                    motor.Rotate(-data.rotateSpeed); // rotates left
                }
                else
                {
                    motor.Move(0);// sets movement to 0
                }
                break;
        }
    }
}

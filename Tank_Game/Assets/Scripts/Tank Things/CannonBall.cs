﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class CannonBall : MonoBehaviour
{
    public float damage;
    public GameObject shooter;
    public GameObject hitObject;

    void OnCollisionEnter(Collision other)
    {
        
         
        hitObject = other.gameObject; // sets hit object to the be the hit object of the cannonball
        if (other.gameObject.GetComponent<TankData>()) // checks if that object has a tank data component
        {
            TankData shooterData = shooter.GetComponent<TankData>(); // sets the object so have cleaner code
            TankData hitObjectData = hitObject.GetComponent<TankData>(); // sets the object so have cleaner code

            if (hitObject.gameObject.tag == "Enemy") // checks if the enemy is being hit
            {
                FXManager.instance.hit.Play();
                hitObjectData.TakeDamage(damage); // makes the tank take damage
                if (hitObjectData.health <= 0) // checks if the health is zero
                {
                    shooterData.AddPoints(hitObjectData.pointValue); // adds points
                }
            }
            if (hitObject.gameObject.tag == "Player") // checks if the enemy is being hit
            {
                FXManager.instance.hit.Play();
                hitObjectData.TakeDamage(damage); // makes the tank take damage
                if (hitObjectData.health <= 0) // checks if the health is zero
                {
                    shooterData.AddPoints(hitObjectData.pointValue); // adds points
                }
            }
        }
        Destroy(gameObject); // die
    }
}

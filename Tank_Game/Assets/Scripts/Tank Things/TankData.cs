﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TankData : MonoBehaviour
{
    public float moveSpeed; // moveSpeed
    public float reverseSpeed; // reverseSpeed
    public float rotateSpeed; // rotateSpeed
    public float shellSpeed = 1f; // shellSpeed
    public float damageDone = 1f; // damageDone
    public float fireRate = 1f; // fireRate
    public float health; // health
    public float maxHealth = 10.0f; // maxHealth
    public int score; // score
    public int pointValue; // pointValue
    public float decayTime; // decayTime

    public int playerNumber = 1;

    public Text LivesUI;
    public Text ScoreUI;




    void Start() // runs at the start of the program
    {
        health = maxHealth; // sets health to max health
        if(tag == "Player")
        {
            LivesUI.text = health.ToString();
            ScoreUI.text = score.ToString();
        }
    }

    public void TakeDamage(float damage) // function for taking damage
    {
        health -= damage; // forces damage
        if (tag == "Player")
        {
            LivesUI.text = health.ToString();
        }
        if (health <= 0 ) // checks if health is below 0
        {
            if (tag == "Player" && GameManager.instance.numberOfPlayers == 2)
            {
                if (playerNumber == 1)
                {
                    GameManager.instance.p1Dead = true;
                }
                if (playerNumber == 2)
                {
                    GameManager.instance.p2Dead = true;
                }
            }
        Die();
        }
    }

    public void Die() // die
    {
        FXManager.instance.death.Play();

        if(tag == "Player" && GameManager.instance.numberOfPlayers == 1)
        {
            SceneManager.LoadScene(2);
        }

        if (tag == "Player" && GameManager.instance.numberOfPlayers == 2)
        {
            if (GameManager.instance.p1Dead == true && GameManager.instance.p2Dead == true)
            {
                SceneManager.LoadScene(2);
            }
        }

        Destroy(gameObject); // destroys game object
    }

    public void AddPoints(int pointValue) // add points
    {
        score += pointValue; // adds to score with the value of point value
        if (tag == "Player")
        {
            ScoreUI.text = score.ToString();
        }
    }

}

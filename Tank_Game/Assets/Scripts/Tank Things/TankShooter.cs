﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class TankShooter : MonoBehaviour
{
    public GameObject cannonBall;
    public GameObject firePoint;

    private TankData data;
   

    void Start()
    {
        data = gameObject.GetComponent<TankData>();
    }

    public void Shoot()
    {
        GameObject ourCannonBall = Instantiate(cannonBall, firePoint.transform.position, Quaternion.identity); // creates game object and instantiates it
        CannonBall cannonBallComponent = ourCannonBall.GetComponent<CannonBall>(); // makes a variable for the cannonball script
        cannonBallComponent.shooter = data.gameObject; // sets the shooter game object
        cannonBallComponent.damage = data.damageDone; // sets damage 
        cannonBallComponent.GetComponent<Rigidbody>().AddForce(transform.forward * data.shellSpeed, ForceMode.Impulse); // adds force
        Destroy(ourCannonBall, data.decayTime); // decay timer
        FXManager.instance.fire.Play();
    }
}

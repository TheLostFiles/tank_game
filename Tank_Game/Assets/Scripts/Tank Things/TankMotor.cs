﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{
    private CharacterController characterController; // makes a character controller called character controller
    private Transform tf; // sets ip the transform
    private TankData data; // sets up the tank data

    void Start() // runs at the started
    {
        characterController = gameObject.GetComponent<CharacterController>(); // sets the character controller to character controller
        tf = GetComponent<Transform>(); // sets the transform to transform
        data = gameObject.GetComponent<TankData>(); // sets the data to tank data
    }

    public void Move(float Speed) // move
    {
        Vector3 Movement = tf.forward * Speed; // does movement * speed

        characterController.SimpleMove(Movement); // makes the character use simple movement
    }

    public void Rotate(float Speed) // rotate 
    {
        Vector3 rotate = Vector3.up * Speed * Time.deltaTime; // does rotation times speed and time

        tf.Rotate(rotate, Space.Self); // makes the tank rotate in its own space
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget = target - tf.position; // gets the location to turn towards

        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

        if (targetRotation == tf.rotation)
        {
            return false;
        }

        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);

        return true;
    }

}

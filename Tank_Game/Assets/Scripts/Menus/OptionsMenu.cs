﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Slider musicSlider;
    public Slider fxSlider;
    public TMPro.TMP_InputField Seed;

    private void Start()
    {
        musicSlider.value = PlayerPrefs.GetFloat("VolumeSlider");
        fxSlider.value = PlayerPrefs.GetFloat("FxSlider");
    }

    public void ClickSound()
    {
        FXManager.instance.click.Play();
    }
    public void SetMusicVolume(float musicVolume)
    {
        audioMixer.SetFloat("MusicVolume", Mathf.Log10(musicVolume) * 20);

        PlayerPrefs.SetFloat("VolumeSlider", musicVolume);
    }

    public void SetFXVolume(float fxVolume)
    {
        audioMixer.SetFloat("FXVolume", Mathf.Log10(fxVolume) * 20);

        PlayerPrefs.SetFloat("FxSlider", fxVolume);
    }

    public void SetMapType(int value)
    {
        if (value == 0)
        {
            GameManager.instance.mapType = GameManager.MapType.Random;
            Seed.enabled = false;
        }

        if (value == 1)
        {
            GameManager.instance.mapType = GameManager.MapType.MapOfTheDay;
            Seed.enabled = false;
        }

        if (value == 2)
        {
            GameManager.instance.mapType = GameManager.MapType.Seeded;
            Seed.enabled = true;
        }

    }

    public void SeedChanged()
    {
        GameManager.instance.mapSeed = Convert.ToInt32(Seed.text);
    }
}

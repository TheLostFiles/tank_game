﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class EndScript : MonoBehaviour
{
    public AudioMixer audioMixer;

    public void Menu()
    {
        SceneManager.LoadScene(0);

    }
    public void ClickSound()
    {
        FXManager.instance.click.Play();
    }

    public void QuitGame()
    {
        Debug.Log("QUIT!!");
        Application.Quit();
    }
}

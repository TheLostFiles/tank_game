﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class FXManager : MonoBehaviour
{
    public static FXManager instance;

    public AudioSource click;

    public AudioSource death;

    public AudioSource fire;

    public AudioSource hit;

    public AudioSource pickup;

    void Awake() // 
    {
        if (instance == null) // checks if instance is null
        {
            instance = this; // makes instance this
        }
        else
        {
            Debug.LogError("ERROR: There can only be one FXManager."); //display message
            Destroy(gameObject); // it go boom
        }
        DontDestroyOnLoad(gameObject); // makes sure it doesn't die when you move scenes
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    public List<PowerUps> PowerUps;
    private TankData data;

    // Start is called before the first frame update
    void Start()
    {
       PowerUps = new List<PowerUps>(); // makes new list
        data = GetComponent<TankData>(); // gets tank data
        
    }

    // Update is called once per frame
    void Update()
    {
        List<PowerUps> ExpiredPowerUps = new List<PowerUps>(); // list for the dead powerups

        foreach (PowerUps power in PowerUps) 
        {
            power.Duration -= Time.deltaTime; // timer things

            if (power.Duration <= 0)
            {
                ExpiredPowerUps.Add(power); // adds dead power up if it has died
            }
        }

        foreach (PowerUps power in ExpiredPowerUps) 
        {
            power.OnDeactivate(data); 
            PowerUps.Remove(power); //once the deactivate they disappear
        }
        ExpiredPowerUps.Clear(); // clears the list
    }

    public void Add(PowerUps powerUp) // adds power ups
    {
        powerUp.OnActivate(data);
        if (!powerUp.IsPermanent) // unless they are permanent
        {
            PowerUps.Add(powerUp); 
        }
    }
}

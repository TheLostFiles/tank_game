﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

[System.Serializable]

public class PowerUps
{
    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float fireRateModifier;

    public float Duration;
    public bool IsPermanent;

    public void OnActivate(TankData data)
    {
        
        data.moveSpeed += speedModifier; // All of these do a adding and subtracting in order to get things done
        data.health += healthModifier;
        if(data.health > data.maxHealth)
        {
            data.health = data.maxHealth;
        }
        if (data.tag == "Player")
        {
            data.LivesUI.text = data.health.ToString();
        }
        data.maxHealth += maxHealthModifier;
        data.fireRate += fireRateModifier;
    }

    public void OnDeactivate(TankData data)
    {
        data.moveSpeed -= speedModifier;
        data.health -= healthModifier;
        if (data.tag == "Player")
        {
            data.LivesUI.text = data.health.ToString();
        }
        data.maxHealth -= maxHealthModifier;
        data.fireRate -= fireRateModifier;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour
{
    public PowerUps PowerUp;
    public AudioClip FeedBackAudioClip;
    private Transform tf;
    

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnTriggerEnter(Collider other)
    {
        // variable to store other object's PowerupController - if it has one
        PowerUpController powCon = other.GetComponent<PowerUpController>();

        // If the other object has a PowerupController
        if (powCon != null)
        {
            // Add the powerup
            powCon.Add(PowerUp);

            FXManager.instance.pickup.Play();

            // Destroy this powerup
            Destroy(gameObject);
        }
    }
}
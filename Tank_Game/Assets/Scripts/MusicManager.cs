﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;

    public AudioSource musicSource;



    void Awake() // 
    {
        if (instance == null) // checks if instance is null
        {
            instance = this; // makes instance this
        }
        else
        {
            Debug.LogError("ERROR: There can only be one MusicManager."); //display message
            Destroy(gameObject); // it go boom
        }
        DontDestroyOnLoad(gameObject); // makes sure it doesn't die when you move scenes
    }
}

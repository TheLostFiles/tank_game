﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class MapGenerator : MonoBehaviour
{
    

    public int rows;
    public int columns;

    public float roomWidth = 50f;
    public float roomHeight = 50f;

    

    public GameObject[] gridPrefabs;

    private Room[,] grid;
    void Start()
    {
        GameManager.instance.levelObject = this.gameObject; // level object
    }
    void Update()
    {
        
    }

    public void StartGame()
    {
        switch (GameManager.instance.mapType)
        {
            case GameManager.MapType.Random: // makes the map seed set to a random seed due to time changing
                GameManager.instance.mapSeed = DatetoInt(DateTime.Now);
                break;
            case GameManager.MapType.Seeded: // type whatever you'd like
                break;
            case GameManager.MapType.MapOfTheDay: // takes just the day makes a seed out of it just for that day
                GameManager.instance.mapSeed = DatetoInt(DateTime.Now.Date);
                break;
            default:
                Debug.Log("[MapGenerator] Map type not implemented");
                break;
        }


        GenerateGrid();
        GameManager.instance.SpawnPlayer(GameManager.instance.RandomSpawnPoint(GameManager.instance.playerSpawnPoints));
        GameManager.instance.SpawnEnemies();
    }


    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)]; // makes a random room
    }

    
    public void GenerateGrid()
    {
        UnityEngine.Random.seed = (GameManager.instance.mapSeed); // makes a random seed the map seed
        grid = new Room[columns,rows]; // makes a new room off of the rid
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < columns; col++)
            {
                float xPosition = roomWidth * col; // makes the room width 
                float zPosition = roomHeight * row; // makes the room height
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity); // makes a new room appear

                tempRoomObj.transform.parent = this.transform; // makes temp room the parent
                tempRoomObj.name = "Room_" + col + ", " + row; // give it a name

                Room tempRoom = tempRoomObj.GetComponent<Room>(); // grabs the component
                grid[col, row] = tempRoom; //makes the temp room size


                ////////////////////////////////////////////////////
                ////These change which doors are open and closed////
                ////////////////////////////////////////////////////
                
                if (row == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (row == rows - 1)
                {
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }

                if (col == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (col == columns - 1)
                {
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }
            }
        }
    }

    // Grabs the whole date down to the millisecond in order to give us a seed
    public int DatetoInt(DateTime datetouse)
    {
        return datetouse.Year + datetouse.Month + datetouse.Day + datetouse.Hour + 
               datetouse.Minute + datetouse.Second + datetouse.Millisecond;
    }
}

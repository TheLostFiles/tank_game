﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SplitScreenCamera : MonoBehaviour
{

    private Camera playerCamera;
    public TankData data;
    // Start is called before the first frame update
    void Start()
    {

        playerCamera = gameObject.GetComponent<Camera>();

        if (GameManager.instance.numberOfPlayers > 1)
        {
            if (GameManager.instance.numberOfPlayers == 2)
            {
                if (data.playerNumber == 1)
                {
                    playerCamera.rect = new Rect(0, 0.5f, 1, 0.5f);
                }

                if (data.playerNumber == 2)
                {
                    playerCamera.rect = new Rect(0, 0, 1, 0.5f);
                }
            }
        }

    }
}
